
var Utils = function(){
     Utils.lastVisibleBody = null;
     Utils.lastHighlightedHead = null;
};

Utils.activateTabs = function(tabheaddiv,tabbodydiv,activeHeadNumber){
      //TODO: add callback functionality to run validation logic before tab change
      var idx = 0;
      var bodyArr = [];
      
      $('#'+tabbodydiv).children().each(function(e){
          $(this).css('display','none');
          if(e==activeHeadNumber){
              $(this).css('display','block');
              Utils.lastVisibleBody = this;
          }
          bodyArr.push(this);
      });
      
      
      
      $('#'+tabheaddiv).children().each(function(e){
          //console.log(this);
          $(this).removeClass('active');
          if(e==activeHeadNumber){
              Utils.lastHighlightedHead = this;
              $(this).addClass('active');
          }
          $(this).on('click',function(){
              
            $(Utils.lastVisibleBody).css('display','none');
            $(Utils.lastHighlightedHead).removeClass('active');
            
            $(bodyArr[e]).css('display','block');
            $(this).addClass('active');
            
            Utils.lastVisibleBody = bodyArr[e];
            Utils.lastHighlightedHead = this;
            
          });
      });
      
}

Utils.aggregateInputDataFromParent = function(formObj){
    
    var retVal = {};
    $(formObj).find("input").each(function(){
        retVal[$(this).attr('name')] = $(this).val();
    });
    
    $(formObj).find("select").each(function(){
        retVal[$(this).attr('name')] = $(this).val();
    });
    
    return retVal;
}




Utils.initiateVerifyEmailModal = function(modalID){
    
    $('#verifyEmailRequest').on('click',function(){
        var data = Utils.aggregateInputDataFromParent($('#'+modalID));
        //send ajax call with data.....[EmailAddress]
        console.log(data);
        $.ajax({url:'verifyEmailAddress',data:data,success:function(data){
                alert(data['message']);     
                $('#'+modalID).modal('hide');
            }
        });
    });
    
    $('#'+modalID).on('hidden.bs.modal', function () {
            $('#'+modalID+' input').each(function(){
              $(this).val("");
            });
        
            $('#'+modalID+' textarea').each(function(){
              $(this).val("");
            });
    });
    
}