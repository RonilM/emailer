//TODO: add verification logic for each form
//TODO: Templates and image uploads for tinyMCE
//TODO: Unsubscribe append at end of email and logic

var EasyDashboardClass = function(){
    
    var currentInstance = this;
    var tinyMceCallbackVals = {};
    
    EasyDashboardClass.prototype.initiate = function(){
        initiateEmailListFormPartial();
        initiateEditorPartial();
        initiateSchedulePartial();
    }
    
    var sendRequestForDispatch = function(){
        var retVal = AggregateDataForEmailDelivery();
        $.ajax({url:'DispatchEmailSendRequest',data:retVal}).done(function(retVal){
                alert(retVal['message']);
        });
    }
    
    var AggregateDataForEmailDelivery = function(){
        var retVal = Utils.aggregateInputDataFromParent($("#describeEmailForm"));
        //console.log(retVal);
        retVal["EmailLists"] = [];
        $('#EmailListTableBody input[type="checkbox"]:checked').each(function(){
            retVal["EmailLists"].push($(this).val());
        });
        retVal["EmailContent"] = tinymce.get('editorDD').getContent();
        retVal["DeliveryType"] = $("#deliveryOptions").val();
        
        if(retVal["DeliveryType"] == 2){
            retVal["ScheduledDeliveryData"] = Utils.aggregateInputDataFromParent($("#deliveryOptionsConfig"));
        }
        
        console.log(retVal);
        
        return retVal;
        
    }
    
    var initiateEmailListFormPartial = function(edc){    
        $('.addContactListCollapse').each(function(){
            updateEmailListOnClick($(this).find('.updateContactsButton'),$(this).find('.emailListTextArea'),$(this).find('.groupID').val());
        });
        
        $('#AddEmailListModal').on('shown.bs.modal', function () {
          //$('#myInput').focus()
        });
        
        $('#AddEmailListModal').on('hidden.bs.modal', function () {
            $('#AddEmailListModal input').each(function(){
              $(this).val("");
            });
        
            $('#AddEmailListModal textarea').each(function(){
              $(this).val("");
            });
        });
        
        addEmailListOnClick($('#AddEmailListButton'),$('#AddEmailListModal').find('textarea[name="EmailList"]'),$('#AddEmailListModal').find('input[name="GroupName"]'));
        
        
    }
    
    var initiateEditorPartial = function(edc){
        
         tinymce.init({ 
             selector:'#editorDD',
             plugins: 'legacyoutput pagebreak anchor visualblocks image textcolor colorpicker advlist link lists table paste media table template',
             toolbar1: 'undo redo pagebreak anchor hr visualblocks styleselect alignleft aligncenter alignright paste bullist numlist image imagetools link media table',
             toolbar2:'sizeselect | bold italic | fontselect |  fontsizeselect template outdent indent code forecolor backcolor',
             menubar: 'edit insert view format table',
             height : 350,
             automatic_uploads: true,relative_urls : false,
             remove_script_host : false,
             convert_urls : true,
             //file_picker_callback: function(field_name, url, type, win) {alert("Placeholder");}
            //images_upload_url: 'postAcceptor.php',
            //images_upload_base_path: '/some/basepath',
            images_upload_credentials: true,
            file_browser_callback: function(field_name, url, type, win) {
                if(type=='image'){ 
                    $('#tinyMceImageForm input').click();
                    tinyMceCallbackVals['win'] = win;
                    tinyMceCallbackVals['field_name'] = field_name;
                }
            }
         });
    
    }
    
    EasyDashboardClass.prototype.OnImageSelect = function(){
        var formData = new FormData(document.getElementById("tinyMceImageForm"));
        $.ajax({
            type: "POST",
            url: 'uploadImage',
            data: formData, // serializes the form's elements.
            processData: false,
            contentType: false,
            success: function(data)
            {
                var getUrl = window.location;
                var baseUrl = getUrl .protocol + "//" + getUrl.host;
                tinyMceCallbackVals['win'].document.getElementById(tinyMceCallbackVals['field_name']).value = baseUrl+data.path;
                //$('#tinyMceImageForm input').value= '';
                console.log(data);
            }
         });

        
        
        
    }
    
    var updateEmailListOnClick = function(buttonObj,boxObj,GroupID){
        $(buttonObj).click(function(e){
            //verify inputs
            var data = {};
            data['EmailList'] = $(boxObj).val().split("\n");
            data['id'] = GroupID;
            $.ajax({url:'addContactList',data:data}).done(function(msg){
                //console.log(msg);
                //clickActionSuccessCallback();
                reloadEmailLists();
            });
        });
    }
    
    var addEmailListOnClick = function(buttonObj,boxObj,GroupObj){
        $(buttonObj).click(function(e){
            //verify inputs
            var data = {};
            data['EmailList'] = $(boxObj).val().split("\n");
            data['id'] = "";
            data['GroupName'] = $(GroupObj).val();
            //console.log(data);
            //console.log("****");
            $.ajax({url:'addContactList',data:data}).done(function(msg){
                
                reloadEmailLists();
                $('#AddEmailListModal').modal('hide');
                //clickActionSuccessCallback();
                //console.log(msg);
            });
        });
    }
    
    var initiateSchedulePartial = function(){
        $('#deliveryOptions').change(function(e){
            if($(this).val() == 2){
                $('#deliveryOptionsConfig').css('display','block');
            }
            else{
                $('#deliveryOptionsConfig').css('display','none');
            }
        });
        
        $("#SubmitRequest").on('click',function(){
            sendRequestForDispatch();
        });
    }
    
    var reloadEmailLists = function(){
        //TODO: finish this function
        $('#EmailListTableBody').html("");
        
    }
    
    
}