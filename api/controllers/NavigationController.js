/**
 * NavigationController
 *
 * @description :: Server-side logic for managing Navigations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req,res){
	    
	    return res.view();
	}
};

