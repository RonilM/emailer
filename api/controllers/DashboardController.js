/**
 * DashboardController
 *
 * @description :: Server-side logic for managing Dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req,res){
	    return res.view();
	},
	
	regularEmailCampaign: function(req,res){
	    return res.view();
	},
	
	easyToUseEditor: function(req,res){
		var emailService = new AmazonSESService();
		var retVal = {};
		
		ProtocolDependantData.findOne({UserID:req.session.UserId}).exec(function(findOneErr,findOneData){
			if(findOneData !=null){
				//console.log(findOneData.QueuedForVerificationEmails);
				emailService.checkIfVerified(findOneData.QueuedForVerificationEmails,function(verErr,verData){
					
					//console.log(verData);
					for(var v in findOneData.QueuedForVerificationEmails){
						var verAttr = verData.VerificationAttributes[findOneData.QueuedForVerificationEmails[v]];
						if(verAttr == null){
							console.log("Invalid Queue Entry");
							findOneData.QueuedForVerificationEmails[v] = "-1";
							continue;
						}
						if(verAttr != null && verAttr.VerificationStatus.toLowerCase() == 'Success'.toLowerCase()){
							findOneData.VerifiedEmails.push(findOneData.QueuedForVerificationEmails[v]);
							findOneData.QueuedForVerificationEmails[v] = "-1";
						}
					}
					
					for(var i = findOneData.QueuedForVerificationEmails.length - 1; i >= 0 ; i--){
						if(findOneData.QueuedForVerificationEmails[i] == "-1"){
							findOneData.QueuedForVerificationEmails.splice(i,1);
						}
					}
					
					ProtocolDependantData.update({UserID:req.session.UserId},{UserID:req.session.UserId,VerifiedEmails:findOneData.VerifiedEmails,QueuedForVerificationEmails:findOneData.QueuedForVerificationEmails}).exec(function(updateErr,updatedData){
							//console.log(updatedData);
							Utilities.getEmailListRenderedViewString(req,res,function(error,rowData){
								retVal["verifiedEmails"] = updatedData[0].VerifiedEmails;
								//console.log(retVal["verifiedEmails"]);
								retVal["emailListRows"] = rowData;
								return res.view(retVal);
					});
							
						});
				});
			}
			else{
				console.log("Cannot find protocol data!");	
			}
		});	
	},
	
	getEmailListRenderedViewString: function(req,res){
		Utilities.getEmailListRenderedViewString(req,res,function(error,rowData){
			var retVal = {};
			retVal["renderedRowView"] = rowData;
			return res.json(retVal);
		});
	},
	
	addContactList: function(req,res){
		var params = req.allParams();
		ContactList.findOne({id:params.id}).exec(function createCB1(err1,data){
			if(data == null){
				ContactList.create({UserID:req.session.UserId,EmailList:params.EmailList,GroupName: params.GroupName}).exec(function createCB2(err2, created){
					if (err2) return res.json(err2);
					return res.json(created);
					
				});
			}
			else{
				ContactList.update({id:params.id,UserID:req.session.UserId},{UserID:data.UserID,EmailList:params.EmailList,GroupName:data.GroupName}).exec(function createCB2(err2, created){
					if (err2) return res.json(err2);
					return res.json(created);
				});
			}
		});
	},
	
	verifyEmailAddress: function(req,res){
		var emailService = new AmazonSESService();
		var params = req.allParams();
		emailService.verifyEmailAddress(params['EmailAddress'],function(err, data) {
			var retVal = {};
        	if (err) {
        		console.log(err, err.stack); 
        		retVal['message'] = JSON.stringify(err.stack);
        		return res.json(retVal);
        	}
        	else{
        		
        		retVal['message'] = "Please complete the verification procedure by logging into the email account and clicking on the verification link. "
        							+"After you click on it, your email will be validated and available shortly. Refersh this page to check if email has"
        							+"been added to the list of validated addresses.";
        		
        		ProtocolDependantData.findOne({UserID:req.session.UserId}).exec(function(err,record){
        			if(record == null){
        				console.log("No Protocol data");
        			}
        			else{
        				var tempWaitQ = record.QueuedForVerificationEmails[record.QueuedForVerificationEmails.length] = params['EmailAddress'];
        				ProtocolDependantData.update({UserID:req.session.UserId},{UserID:req.session.UserId,VerifiedEmails:record.VerifiedEmails,QueuedForVerificationEmails:tempWaitQ}).exec(function(err1,created){
        					if (err) {console.log(err, err.stack);  
        						retVal['message'] = JSON.stringify(err.stack);
        					}
        					return res.json(retVal);
        				});
        				
        			}
        		});
        							
        	}
      	});
		
	},
	
	DispatchEmailSendRequest: function(req,res){
		var retVal = {};
		retVal['message'] = 'Request for dispatch has been successfully submitted!';
		var params = req.allParams();
		
		var emailService = new AmazonSESService();
		
		if(params.DeliveryType == '1'){
			
			var emailLists = params.EmailLists;
			
			ContactList.find({id:{ $in:emailLists},UserID:req.session.UserId}).exec(function(err,contactListData){
					
				for(var cld in contactListData){
					if(contactListData[cld].EmailList.length > 200){
						console.log('Contact list too large!');
						continue;
					}
					var par = {};
					par.bccAddressArray = contactListData[cld].EmailList;
					par.ccAddressArray = [];
					par.sendToArray = [];
					par.htmlData = params.EmailContent;
					par.textData = "";
					par.subject = params.Subject;
					
					//TODO: verify source and reply to addresses
					par.sourceAddress = params.FromAddress;
					par.replyToAddresses = [params.ReplyAddress];
					
					emailService.sendEmail(par,function(sendErr,sendData){
						console.log(sendData);
					})
					
				}
					
			});
			
		}
		else if(params.DeliveryType == '2'){
			
		}
		
		return res.json(retVal);
		
	},
	
	uploadImage: function(req,res){
		 res.setTimeout(0);

	    req.file('TinyMceImg').upload({
	
	      //file upload limit (in bytes)
	      maxBytes: 10000000,
	      dirname: sails.config.GlobalVar.tinyMcePublicImagePath
			
	    }, function whenDone(err, uploadedFiles) {
	      if (err){ 
	      	console.log(err);
	      	return res.serverError(err);
	      }
	      else{ 
	      	//console.log(uploadedFiles);
	      	return res.json({path: sails.config.GlobalVar.tineMcePublicPathForUser+'/'+uploadedFiles[0].fd.split("/")[uploadedFiles[0].fd.split("/").length -1]});
	      }
	    });
	}
	
	
};


