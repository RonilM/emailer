/**
 * AuthenticationController
 *
 * @description :: Server-side logic for managing Authentications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	LoginPage: function(req,res){
	    return res.view();
	    
	},
	Authenticate: function(req,res){
		var values = req.allParams();
		UserAuth.findOne({email:values.email,password:values.password}).exec(function(err,result){
			
			if (err) return next(err);
			if(result == null){
				return res.json({'error':'Incorrect username or Password'});
			}
			req.session.authenticated = true;
			req.session.UserId = result.id;
			
			ProtocolDependantData.findOne({UserID:result.id}).exec(function(err2,data2){
				
				if(data2 == null){
					ProtocolDependantData.create({UserID:result.id,VerifiedEmails:[],QueuedForVerificationEmails:[]}).exec(function(){
						return res.redirect('Dashboard/Index');
					});
				}
				else{
					return res.redirect('Dashboard/Index');	
				}
			});
			
			
			//return res.json(result);
		});
		
	}
	
	
};

