/**
 * UserAuth.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    email: {type:'STRING',unique:true,required:true},
    password: 'STRING',
    UserData: {
      collection:'UserData',
      via:'UserID'
    },
    UserUsageData: {
      collection:'UserUsageData',
      via:'UserID'
    },
    ProtocolDependantData: {
      collection:'ProtocolDependantData',
      via:'UserID'
    },
    ContactList:{
      collection:'ContactList',
      via:'UserID'
    },
    EmailTransactions:{
      collection: 'EmailTransactions',
      via:'UserID'
    }
  }
  
  
  
};

