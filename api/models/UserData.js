/**
 * UserData.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    UserID: {model:'userauth', required: true},
    FirstName: {type:'STRING',required:true},
    LastName: {type:'STRING'},
    CompanyName: {type:'STRING'},
    CompanyAddress: {type:'STRING'},
    ContactNumber: {type:'STRING',required:true},
    AlternateContactNumber: {type:'STRING'}
  }
  
};

