module.exports = SendGridService = function(){
      
    var sendgrid  = require('sendgrid')('YOUR_SENDGRID_API_KEY');
      
    SendGridService.prototype.sendEmail = function(params){
        console.log("Sending Mail!");
        params["smtpapi"] = new sendgrid.smtpapi();
        var email = new sendgrid.Email(params);
          
        sendgrid.send(email, function(err, json) {
            if (err) { return console.error(err); }
            console.log(json);
            //Code to update transaction status
        });
        
        return;
    }
      
}