module.exports = {
    
    getEmailListRenderedViewString: function(req,res,callback){
        
        ContactList.find({"UserID":req.session.UserId}).exec(function(err,contactListData) {
		
			if (err) return res.json(err);
			
			for(var cld in contactListData){
				var count = 0;
				for(var i in contactListData[cld].EmailList)
					count++;
				contactListData[cld].EmailCount = count;
			}
			
			var retValInt = {};
			retValInt["contactListData"] = contactListData;
			retValInt["layout"] = null;
			
			sails.renderView('dashboard/partial/partialElements/_emailListRows',retValInt,function(err2,rowData){
			    if (err2) return res.json(err2);
				callback(err2,rowData);
			});
		});
        
    }
    
};
